# Moses
Moses is a low-privilege **Remote Access Trojan** written in the **Python** programming language targeting **Windows** systems.

# Functions
Moses contains functions such as screenshot target computers, capture webcam photo on target computers, open webpage on target computers, execute program on target computers, host web server on target machine, get IP Addresses of all infected computers, and create firewall rules.
All of the mentioned functions that send data use email, but, not all functions send data, as some do not need to.
This gives Moses an "advantage" over other RATs if you're looking for security first.
This is due to you not actually having to connect to the target computer whatsoever (thanks to the "Masquerade 2.0" system architecture).
A byproduct of having security being the priority is you will not have as many monitoring capabilities (such as remote desktop session, real-time video, etc.) on the target system (at this stage in development, at least) as you would normally.
More features coming soon.
## Deployment
Deploy the ```moses.py``` file onto the victim machine(s) and place into the ```C:/Users/USERNAME/AppData/Roaming/``` directory (```USERNAME``` equals placeholder for the current username).

Then, use the ```remote_control_for_linux.py``` as a sort of remote control which sends commands indirectly to the active RAT(s) using a web server as a proxy/intermediary device (Server/Wall architecture).

**Ensure that before you start the controller, correctly set the reciverAddress, senderAddress, and senderPassword variables in moses.py to enable email replies from the RAT(s). Also, ensure that you have set the sshIPAddress, sshUsername, and sshPassword variables in ```remote_control_for_linux.py``` to be able to send commands and update each site on the web server.**

# Operation
Moses is operated through the victim's computer processing three unique webpages hosted on a web server, being ipaddress/tar1.html, ipaddress/tar2.html, and ipaddress/tar3.html.
The remote controller (remote_control_for_linux.py) is natively set up to append commands through SSH into the ```/var/www/Website``` directory found on a web server.
That is where each webpage is found.
Either set up a web server using the mentioned settings, or adapt the RAT to fit your own configuration.
- ```tar1.html``` stores main commands temporarily, such as "DS" for desktopScreenshot function, or "EP" for executeProgram function, for Moses to view and execute
- ```tar2.html``` store sub-commands/sub-values that a function may need depending on the function
- ```tar3.html``` stores sub-sub-values (it's a further version of tar2.html).

The ```remote_control_for_linux.py``` utility is what will momentarily append commands to the webpages.
After (correctly) configuring the ```remote_control_for_linux.py```, the remote will be able to send out a command which will be appended to the first HTML webpage (ipaddress/tar1.html) located on the web server.
Site one (```tar1.html```) is always being monitored by Moses: RAT.
When a change is detected in the webpage contents, the webpage will be downloaded by the RAT, be processed by the RAT, and finally executed by the RAT.
Depending on the command sent, more sub-values could have been sent to the other two webpages, so the RAT may download the others as well.
After 10 seconds, the page(s) is once again wiped back to it's original blank state by the remote.
This allows for the page to only be viewable for 10 seconds.

# Dependencies
- Ensure that you have these Python modules installed on the victim machine(s) to run moses.py
	- ```pip3 install pyautogui opencv-python playsound psutil```
- Ensure that you have these dependencies installed for the usage of ```remote_control_for_linux.py``` (Linux only)
	- For Debian-based distributions
		- ```sudo apt install sshpass```
	- For Arch-based distributions
		- ```sudo pacman -S sshpass```

# Legal Disclaimer
All tools provided by this project are for research and study purposes only.
You must obey the laws and regulations of your country during use.
I will not be held responsible for any misuse that may arise during the usage of this project.

# TODO
- [ ] Move try blocks into ```main()``` instead of directly in the function (this will allow me to reuse send_email() by manipulating vars to demonstrate failed)
- [ ] Obfuscate the main command names (such as "DS") by generating random strs and using those instead (could prevent prying eyes)
- [ ] Ensure less dependency on modules and design effective ways to operate functions without them
- [ ] Create a "timeout" switch after the command is sent
- [ ] Complete ```open_ports_in_firewall()``` function
- [ ] Command executed/failed feedback function
- [ ] Reduce redundant code 

# DONE
- [X] Convert everything to snake case
